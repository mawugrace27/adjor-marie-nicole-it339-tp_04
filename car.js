const p = require('./person');

//definition de la classe Voiture
class Voiture extends p.Personne {

    //constructeur de la classe voiture
    constructor(nom,sexe,age,marque,modele,annee) {
        //attribut de la classe mere
        super(nom,sexe,age);
        
        //initialisation des attributs de la classe
        this.marque = marque;
        this.modele = modele;
        this.annee = annee;        
    }

    //getters et setteres
    //marque
    getMarque(){
        return this.marque;
    }
    setMarque(newMarque){
        this.marque = newMarque;
    }
    //modele
    getMarque(){
        return this.modele
    }
    setMarque(newModele){
        this.modele = newModele;
    }
    //annee
    getAnnee(){
        return this.annee;
    }
    setMarque(newAnnee){
        this.annee = newAnnee;
    }

    //methode AfficherVoiture
    afficherVoiture(){
        var result = "Marque :"+this.marque+" Modele : "+this.modele+" Annee : "+this.annee+" NomPropriétaire : "+p.person1.getNom()+" Sexe :"+p.person1.getSexe()+" Age :"+p.person1.getAge();
        return result;
    }
}

// 5) instance la classe voiture
var car1 = new Voiture("John","Masculin", 21,"Audi","Avus Quatro", 2005);

//affichage des proprietaire de l'instance 
console.log(car1.afficherVoiture());

module.exports = {Voiture};