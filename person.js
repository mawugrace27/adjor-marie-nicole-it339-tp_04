// definition du modele person

class Personne{
    // constructeur
    constructor(nom,sexe,age) {
        // initialisation des attributs
        this.name = nom;
        this.sexe = sexe;
        this.age = age;
    }

    //getters et setters
    //nom
    getNom(){
        return this.name;
    }
    setNom(newNom){
        this.name = newNom;
    }
    //sexe
    getSexe(){
        return this.sexe;
    }
    setSexe(newSexe){
        this.sexe = newSexe;
    }
    //age
    getAge(){
        return this.age;
    }
    setAge(newAge){
        this.name = newAge;
    }
}

// 2) instance de la classer Personne
var person1 = new Personne("Root","Masculin",16);

//affichage de l'objet ci dessus
console.log("Nom : "+person1.name+" Sexe : "+person1.genre+" Age : "+person1.years);

module.exports= { Personne , person1};